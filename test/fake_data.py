ENGLISH_SENTENCES = [
    "Hi, this a great python package don't you think ?",
    "I'm a great fan of this package",
    "It can be used to embed sentences",
    "And even have some option to select the device to use",
    "It's also possible to use it with a CPU",
    "And to use them for bitext filtering and mining",
]

FRENCH_SENTENCES = [
    "Salut, c'est un super package python, tu ne trouves pas ?",
    "Je suis un grand fan de ce package",
    "Il peut être utilisé pour encoder des phrases",
    "Et même avoir des options pour sélectionner le device à utiliser",
    "Il est aussi possible de l'utiliser avec un CPU",
    "Et de les utiliser pour filtrer et miner des bitextes",
]
